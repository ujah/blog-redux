import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fetchUser} from '../actions'

class UserHeader extends Component {
    componentDidMount(){
        this.props.fetchUser(this.props.userid);
    }
    
    render() { 
        const {user} = this.props;
        if (!user){
            return null;
        }
        return ( 
            <div className="header"> {user.name} </div>
         );  
    }
}
 
const mapStateToProps= (state, ownProps) => {
    return{ user: state.user.find(user=>user.id === ownProps.userid)};
 }
  
 export default connect(mapStateToProps, {fetchUser})(UserHeader);